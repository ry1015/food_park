from django.apps import AppConfig


class FoodTrucksConfig(AppConfig):
    name = 'food_trucks'

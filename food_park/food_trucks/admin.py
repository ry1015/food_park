from django.contrib import admin
from .models import Address, Category, Currency, Menu, MenuImage, Restaurant, RestaurantImage, Rating

# Register your models here.
models = {
  Address,
  Category,
  Currency,
  Menu,
  MenuImage,
  Restaurant,
  RestaurantImage,
  Rating,
}

admin.site.register(models)

from django.contrib.auth.models import User
from rest_framework import permissions, status, viewsets
from rest_framework.response import Response
from .user_serializer import UserSerializer, UserValidateSerializer

class AnonListOnly(permissions.BasePermission):
  """
  Custom permission:
    - allow anonymouse to READ
  """

  def has_permission(self, request, view):
    return view.action == 'list'

class AdminPermissions(permissions.BasePermission):
  """
  Allow admins full access
  """
  def has_object_permission(self, request, view, obj):
    return view.action in ['list', 'retrive', 'update', 'partial_update', 'delete'] and obj.user.id == request.user.id

class UserViewSet(viewsets.ModelViewSet):
  """Create, modify, retrieve, and delete food truck information
  """
  queryset = User.objects.all()
  serializer_class = UserSerializer
  http_method_names = ['post']
  
  def create(self, request, *args, **kwargs):
    # serializer = self.get_serializer(data=request.data)
    try:
      user = User.objects.get(username=request.data.get("username"))
    except:
      return Response("User does not exist.", status=status.HTTP_400_BAD_REQUEST)

    if user.check_password(request.data.get("password")):
      serializer = self.get_serializer(user)
      headers = self.get_success_headers(serializer.data)
      return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
    else:
      return Response("Wrong password", status=status.HTTP_400_BAD_REQUEST)    

  # def list(self, request, *args, **kwargs):
  #   print(self.request.data)
  #   queryset = self.filter_queryset(self.get_queryset())

  #   page = self.paginate_queryset(queryset)
  #   if page is not None:
  #     serializer = self.get_serializer(page, many=True)
  #     return self.get_paginated_response(serializer.data)

  #   serializer = self.get_serializer(queryset, many=True)
  #   return Response(serializer.data)
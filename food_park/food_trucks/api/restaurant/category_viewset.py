from rest_framework import viewsets
from food_trucks.models import Category
from .restaurant_serializer import RestaurantCategorySerializer

class CategoryViewSet(viewsets.ModelViewSet):
  """Create, modify, retrieve, and delete restaurant address information
  """
  queryset = Category.objects.all()
  serializer_class = RestaurantCategorySerializer
  http_method_names = ['post', 'delete']
  
from django.contrib.auth.models import User
from rest_framework import permissions, serializers, viewsets
from food_trucks.models import Address
from .restaurant_serializer import AddressSerializer
from pprint import pprint

class AddressViewSet(viewsets.ModelViewSet):
  """Create, modify, retrieve, and delete restaurant address information
  """
  queryset = Address.objects.all()
  serializer_class = AddressSerializer
  http_method_names = ['post', 'delete']
  
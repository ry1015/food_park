from rest_framework import serializers
from food_trucks.models import Address, Category, Currency, Menu, MenuImage, Restaurant, RestaurantImage

class CurrencySerializer(serializers.ModelSerializer):

  class Meta:
    model = Currency
    fields = ('symbol',)

class MenuImageSerializer(serializers.ModelSerializer):
  image = serializers.SerializerMethodField()

  class Meta:
    model = MenuImage
    fields = ('image',)

  def get_image(self, obj):
    filename_arr = str(obj.image).split('/')
    size = len(filename_arr)
    return filename_arr[size-1]

class RestaurantImageSerializer(serializers.ModelSerializer):
  image = serializers.SerializerMethodField()

  class Meta:
    model = RestaurantImage
    fields = ('image',)

  def get_image(self, obj):
    filename_arr = str(obj.image).split('/')
    size = len(filename_arr)
    return filename_arr[size-1]

class RestaurantAddressSerializer(serializers.ModelSerializer):
  
  class Meta:
    model = Address
    fields = ('id', 'city', 'country')

class RestaurantCategorySerializer(serializers.ModelSerializer):
  
  class Meta:
    model = Category
    fields = '__all__'

class RestaurantMenuSerializer(serializers.ModelSerializer):
  menu_image = MenuImageSerializer(many=True)

  class Meta:
    model = Menu
    fields = ('id', 'category', 'name', 'price', 'restaurant', 'currency', 'menu_image')

class RestaurantCategorySerializer(serializers.ModelSerializer):
  
  class Meta:
    model = Category
    fields = '__all__'

class AddressSerializer(serializers.ModelSerializer):
  
  class Meta:
    model = Address
    fields = '__all__'

class CategorySerializer(serializers.ModelSerializer):
  
  class Meta:
    model = Category
    fields = ('id', 'name')

class RestaurantSerializer(serializers.ModelSerializer):
  categories = CategorySerializer(many=True)
  addresses = RestaurantAddressSerializer(many=True)
  restaurant_image = RestaurantImageSerializer()

  class Meta:
    model = Restaurant
    fields = ('id', 'name', 'categories', 'addresses', 'restaurant_image', 'status')

class MenuSerializer(serializers.ModelSerializer):
  currency = CurrencySerializer(read_only=True)

  class Meta:
    model = Menu
    fields = ('id', 'name', 'currency', 'price')

class RestaurantDetailSerializer(serializers.ModelSerializer):
  categories = CategorySerializer(many=True)
  addresses = RestaurantAddressSerializer(many=True)
  menus = MenuSerializer(many=True)
  restaurant_image = RestaurantImageSerializer()

  class Meta:
    model = Restaurant
    fields = ('id', 'name', 'categories', 'menus', 'addresses', 'restaurant_image', 'status')

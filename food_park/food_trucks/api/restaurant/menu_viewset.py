from django.contrib.auth.models import User
from rest_framework import status, viewsets
from rest_framework.response import Response
from food_trucks.models import Currency, Menu
from .restaurant_serializer import RestaurantMenuSerializer
from pprint import pprint

class MenuViewSet(viewsets.ModelViewSet):
  """Create, modify, retrieve, and delete restaurant address information
  """
  queryset = Menu.objects.all()
  serializer_class = RestaurantMenuSerializer
  # http_method_names = ['post', 'delete']
  
  def create(self, request, *args, **kwargs):
    new_request = request.data
    request_country = new_request.pop('country', None)

    if request_country is None:
      return Response("Country field is missing.", status=status.HTTP_400_BAD_REQUEST)
    else:
      currency = Currency.objects.get(country=request_country)
      new_request['currency'] = currency.id
      serializer = self.get_serializer(data=new_request)
      serializer.is_valid(raise_exception=True)
      self.perform_create(serializer)
      headers = self.get_success_headers(serializer.data)
      return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

from django.contrib.auth.models import User
from rest_framework import status, viewsets
from rest_framework.response import Response
from food_trucks.models import Restaurant, RestaurantImage
from .restaurant_serializer import RestaurantImageSerializer
from pprint import pprint

class RestaurantImageViewSet(viewsets.ModelViewSet):
  """Create, modify, retrieve, and delete restaurant address information
  """
  queryset = RestaurantImage.objects.all()
  serializer_class = RestaurantImageSerializer
  # http_method_names = ['post', 'delete']
  
  def create(self, request, *args, **kwargs):
    restaurant = Restaurant.objects.get(id=request.data['restaurant'])
    obj, created = RestaurantImage.objects.update_or_create(restaurant=restaurant, defaults={'image':request.FILES.get('image')})
    serializer = RestaurantImageSerializer(obj)

    if created:
      headers = self.get_success_headers(serializer.data)
      return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
    else:
      return Response(serializer.data)


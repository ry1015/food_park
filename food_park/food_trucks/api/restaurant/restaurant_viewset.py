from django.contrib.auth.models import User
from rest_framework import permissions, serializers, viewsets
from food_trucks.models import Restaurant
from .restaurant_serializer import RestaurantSerializer, RestaurantDetailSerializer

class AnonListOnly(permissions.BasePermission):
  """
  Custom permission:
    - allow anonymouse to READ
  """

  def has_permission(self, request, view):
    return view.action == 'list' or request.user and request.user.is_authenticated

# class AdminPermissions(permissions.BasePermission):
#   """
#   Allow admins full access
#   """
#   def has_object_permission(self, request, view, obj):
#     return view.action in ['list', 'retrive', 'update', 'partial_update', 'delete'] and obj.user.id == request.user.id

class RestaurantViewSet(viewsets.ModelViewSet):
  """Create, modify, retrieve, and delete restaurant information
  """
  queryset = Restaurant.objects.all()
  serializer_class = RestaurantSerializer
  permission_classes = (AnonListOnly,)
  action_serializers = {
    'retrieve': RestaurantDetailSerializer
  }

  def get_serializer_class(self):
    if hasattr(self, 'action_serializers'):
      if self.action in self.action_serializers:
        return self.action_serializers[self.action]
    return super(RestaurantViewSet, self).get_serializer_class()

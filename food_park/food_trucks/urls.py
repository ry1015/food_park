from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
    TokenVerifyView
)
from food_trucks.api.restaurant.address_viewset import AddressViewSet
from food_trucks.api.restaurant.category_viewset import CategoryViewSet
from food_trucks.api.restaurant.menu_viewset import MenuViewSet
from food_trucks.api.restaurant.restaurant_viewset import RestaurantViewSet
from food_trucks.api.restaurant.restaurant_image_viewset import RestaurantImageViewSet
from food_trucks.api.user.user_viewset import UserViewSet

router = DefaultRouter(trailing_slash=False)
router.register(r'addresses', AddressViewSet)
router.register(r'categories', CategoryViewSet)
router.register(r'menus', MenuViewSet)
router.register(r'restaurants', RestaurantViewSet)
router.register(r'restaurant_images', RestaurantImageViewSet)
router.register(r'users', UserViewSet)

urlpatterns = [
    url(r'^api/v1/', include(router.urls)),
    url(r'^api/v1/token/obtain$', TokenObtainPairView.as_view(), name='token_obtain'),
    url(r'^api/v1/token/refresh$', TokenRefreshView.as_view(), name='token_refresh'),
    url(r'^api/v1/token/verify$', TokenVerifyView.as_view(), name='token_verify'),
]
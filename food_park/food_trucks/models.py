from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.utils import timezone
import os

# Create your models here.
class Restaurant(models.Model):
  created_date = models.DateTimeField(auto_now_add=True)
  last_modified = models.DateTimeField(auto_now=True)
  name = models.CharField(max_length=200)
  status = models.IntegerField(validators=[MinValueValidator(0),
                                       MaxValueValidator(1)], default=1)

  def __str__(self):
    preview = "%s - %s" % (self.id, self.name)
    return preview

class Address(models.Model):
  city = models.CharField(max_length=100)
  country = models.CharField(max_length=100)
  created_date = models.DateTimeField(auto_now_add=True)
  last_modified = models.DateTimeField(auto_now=True)
  restaurant = models.ForeignKey('Restaurant', related_name='addresses', on_delete=models.CASCADE)

  def __str__(self):
    preview = "%s - %s, %s; %s" % (self.id, self.city, self.country, self.restaurant)
    return preview

class Category(models.Model):
  name = models.CharField(max_length=100)
  created_date = models.DateTimeField(auto_now_add=True)
  last_modified = models.DateTimeField(auto_now=True)
  restaurant = models.ForeignKey('Restaurant', related_name='categories', on_delete=models.CASCADE)

  def __str__(self):
    preview = "%s - %s" % (self.id, self.name)
    return preview

class Currency(models.Model):
  country = models.CharField(max_length=60)
  symbol = models.CharField(max_length=1)
  created_date = models.DateTimeField(auto_now_add=True)
  last_modified = models.DateTimeField(auto_now=True)

  def __str__(self):
    preview = "%s - %s, %s" % (self.id, self.symbol, self.country)
    return preview

class Menu(models.Model):
  category = models.CharField(max_length=200)
  created_date = models.DateTimeField(auto_now_add=True)
  currency = models.ForeignKey('Currency', related_name='menus_currency', on_delete=models.SET_NULL, blank=True, null=True)
  restaurant = models.ForeignKey('Restaurant', related_name='menus', on_delete=models.CASCADE)
  last_modified = models.DateTimeField(auto_now=True)
  name = models.CharField(max_length=100)
  price = models.DecimalField(max_digits=15, decimal_places=2)

  def __str__(self):
    preview = "%s - %s%s - %s; " % (self.id, self.currency.symbol, self.price, self.name)
    return preview

class Rating(models.Model):
  created_date = models.DateTimeField(auto_now_add=True)
  restaurant = models.ForeignKey('Restaurant', on_delete=models.CASCADE)
  last_modified = models.DateTimeField(auto_now=True)
  rating = models.IntegerField()

  def __str__(self):
    preview = "%s - %s stars" % (self.id, self.rating)
    return preview
  
class RestaurantImage(models.Model):
  def get_image_path(instance, filename):
    return os.path.join('static', 'images', 'restaurant', str(instance.restaurant.id), filename)

  created_date = models.DateTimeField(auto_now_add=True)
  last_modified = models.DateTimeField(auto_now=True)
  restaurant = models.OneToOneField('Restaurant', related_name='restaurant_image', on_delete=models.CASCADE)
  image = models.ImageField(upload_to=get_image_path, blank=True, null=True)

  def __str__(self):
    preview = "%s - %s" % (self.id, self.restaurant)
    return preview

class MenuImage(models.Model):
  def get_image_path(instance, filename):
    return os.path.join('static', 'images', 'restaurant', 'menu', str(instance.menu.id), filename)

  created_date = models.DateTimeField(auto_now_add=True)
  last_modified = models.DateTimeField(auto_now=True)
  menu = models.ForeignKey('Menu', related_name='menu_image', on_delete=models.CASCADE, blank=True, null=True)
  image = models.ImageField(upload_to=get_image_path, blank=True, null=True)  

  def __str__(self):
    preview = "%s - %s" % (self.id, self.menu.name)
    return preview

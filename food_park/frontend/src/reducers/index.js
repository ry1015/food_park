import {combineReducers} from 'redux'
import loginReducer from './admin/login'
import restaurantReducer from './admin/body'

export default combineReducers({
  login: loginReducer,
  restaurants: restaurantReducer,
});

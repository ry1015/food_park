import { CLOSE_RESTAURANT_INFO, FETCH_RESTAURANT_INFO, FETCH_RESTAURANTS } from '../../actions/index';

const initialState = {
  restaurants: {},
  restaurant: undefined
};

function restaurantReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_RESTAURANTS:
      return Object.assign({}, state, {restaurants: action.payload})
    case FETCH_RESTAURANT_INFO:
      return Object.assign({}, state, {restaurant: action.payload})
    case CLOSE_RESTAURANT_INFO:
      return Object.assign({}, state, {restaurant: undefined})
    default:
     return state
  }
}

export default restaurantReducer;
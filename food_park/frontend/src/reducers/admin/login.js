import { LOGIN_SUCCESS, TOKEN_VERIFIED } from '../../actions/index';

const initialState = {
  isLoggedIn: false,
  tokenVerified: false
};

function loginReducer(state = initialState, action) {
  switch (action.type) {
    // case FETCH_USER:
    //   return Object.assign({}, state, {status: action.payload.status, isLoggedIn: true})
    case LOGIN_SUCCESS:
      return Object.assign({}, state, {status: action.payload.status, isLoggedIn: true, tokenVerified: true})
    case TOKEN_VERIFIED:
      return Object.assign({}, state, {tokenVerified: true, isLoggedIn: true})
    default:
     return state
  }
}

export default loginReducer;
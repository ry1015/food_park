import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom'
// import logo from './logo.svg';
import './App.css';
import Admin from './components/admin/Admin';
import Index from './components/index/Index';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="App">
            <Switch>
              <Route path="/" exact component={Index} />
              <Route path="/admin" component={Admin} />
            </Switch>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;

import React, { Component } from 'react';
// import logo from './logo.svg';
// import './App.css';
import './css/admin-header.css';

class AdminHeader extends Component {
  render() {
    return (
      <div className="header">
        <div id="header-wrapper">
          <div id="admin-title">Food Park Admin</div>
        </div>
      </div>
    );
  }
}

export default AdminHeader;

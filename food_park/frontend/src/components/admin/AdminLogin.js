import React, { Component } from 'react';
// import logo from './logo.svg';
// import './App.css';
// import AdminHeader from './AdminHeader';
// import AdminBody from './AdminBody';
import './css/admin-login.css';

class AdminLogin extends Component {
  constructor(props) {
    super(props);
    this.state = {username: 'admin', password: 'Spring20!6'};

    this.handleChange = this.handleChange.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    if (event.target.name === 'usr') {
      this.setState({username: event.target.value});  
    } else {
      this.setState({password: event.target.value});  
    } 
  }

  handleKeyPress(event) {
    if (event.key === 'Enter') {
      this.handleSubmit(event);
    }
  }

  handleSubmit(event) {
    this.props.handleSubmit(this.state);
    event.preventDefault();
  }

  render() {
    return (
      <div className="admin-login">
        <form onSubmit={this.handleSubmit} onKeyPress={this.handleKeyPress}>
          <div id="admin-login-body">
            <label>Username</label>
            <input type="text" placeholder="Enter Username" name="usr" value={this.state.username} onChange={this.handleChange} required />

            <label>Password</label>
            <input type="password" placeholder="Enter Password" name="pwd" value={this.state.password} onChange={this.handleChange} required />

            <button type="submit">Login</button>
          </div>
        </form>
      </div>
    );
  }
}

export default AdminLogin;

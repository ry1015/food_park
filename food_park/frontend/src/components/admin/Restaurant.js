import React, { Component } from 'react';
import './css/admin-restaurant.css';

// Parent: AdminBody
class Restaurant extends Component {
  constructor(props) {
    super(props);
    this.displayImage = this.displayImage.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  displayImage() {
    let style = {}

    if (this.props.children.restaurant_image !== null) {
      style = {
        backgroundImage: `url(/api/static/${this.props.children.id}/${this.props.children.restaurant_image.image})`
      }
      return <div className="restaurant-img" style={ style }></div>
    } else {
      style = {
        backgroundImage: "url('/api/static/add_image.png')"
      }
      return <div className="restaurant-img-empty" style={ style }></div>
    }
  }

  handleClick(event) {
    let {handleRestaurantClick, rid} = this.props;
    handleRestaurantClick(rid);
  }

  render() {
    let { name, categories, addresses } = this.props.children;
    let restCategories = '';
    let restAddress = '';
    let className = (this.props.children.status === 1) ? "admin-restaurant" : "admin-restaurant-closed"
    let getImage = this.displayImage();

    categories.map((rest, index) => restCategories += categories[index]['name'] + ", ");
    addresses.map((add, index) => restAddress += addresses[index]['city'] + ", " + addresses[index]['country'] + "; ");
    
    restCategories = restCategories.substring(0, restCategories.length - 2);
    restAddress = restAddress.substring(0, restAddress.length - 2);

    return (
      <div className={className} onClick={this.handleClick}>
        <div className="restaurant-img-container">
          { getImage }
        </div>
        <div className="restaurant-info"><span>{name}</span>
        {
          categories.length < 2 ? (
            <div className="restaurant-category"><strong>Category:</strong> {restCategories} </div>
          ) : (
            <div className="restaurant-category"><strong>Categories:</strong> {restCategories}</div>
          )
        }
        {
          addresses.length < 2 ? (
            <div className="restaurant-category"><strong>Address:</strong> {restAddress} </div>
          ) : (
            <div className="restaurant-category"><strong>Addresses:</strong> {restAddress}</div>
          )
        }
        </div>
      </div>
    );
  }
}

export default Restaurant;

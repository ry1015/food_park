import React, { Component } from 'react';
// import logo from './logo.svg';
// import './App.css';
// import Restaurants from './Restaurants';
import './css/admin-filter.css';

class AdminFilter extends Component {
  render() {
    return (
      <div className="admin-filter">
        <form>
          <div></div>
          <input placeholder="Search for name or city" onChange={(input) => this.props.handleSearch(input)}/>
        </form>
      </div>
    );
  }
}

export default AdminFilter;

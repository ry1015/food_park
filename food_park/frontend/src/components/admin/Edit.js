import React, { Component } from 'react';
import { connect } from "react-redux";
import axios from 'axios';
// import logo from './logo.svg';
// import './App.css';
// import Restaurants from './Restaurants';
import './css/admin-edit.css';

const isEqual = require('deep-equal');

// Parent: RestaurantInfo
class Edit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      arrayObj: this.props.children[this.props.children.name],
      country: this.getCountry() || '',
      category: '',
      price: '',
      name: '',
      menuCategory: ''
    };

    this.getCountry = this.getCountry.bind(this);
    this.getForm = this.getForm.bind(this);
    this.onDelete = this.onDelete.bind(this);
    this.handleCategoryChange = this.handleCategoryChange.bind(this);
    this.handleCountryChange = this.handleCountryChange.bind(this);
    this.handleMenuCategoryChange = this.handleMenuCategoryChange.bind(this);
    this.handleMenuNameChange = this.handleMenuNameChange.bind(this);
    this.handleMenuPriceChange = this.handleMenuPriceChange.bind(this);
    this.trashButton = this.trashButton.bind(this);
    this.getCountry();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (!isEqual(this.state.arrayObj, this.props[this.props.children.name])) {
      this.setState({arrayObj: this.props[this.props.children.name]});
    }
  }

  getCountry() {
    axios.get(
      "http://ip-api.com/json"
    )
    .then(response => {
      this.setState({country: response.data.country});
    })
    .catch(error => {
      this.setState({country: ""});
      throw(error);
    });
  }

  getForm(name) {
    if (name === 'addresses') {
      return  <form onSubmit={this.props.handleSubmit} name={name}>
                  <input type="text" placeholder="Enter City..."/>
                  <input type="text" placeholder="Enter Country..." value={this.state.country} onChange={this.handleCountryChange} required/>
                  <input type="submit" value="Add" />
              </form>
    } else if (name === 'categories') {
      return  <form onSubmit={this.props.handleSubmit} name={name}>
                  <input type="text" placeholder="Enter Category Name..." value={this.state.category} onChange={this.handleCategoryChange} required/>
                  <input type="submit" value="Add" />
              </form>
    } else if (name === 'menus') {
      return  <form onSubmit={this.props.handleSubmit} name={name}>
                  <input type="number" step="0.01" placeholder="Enter Price (0.00)..." value={this.state.price} onChange={this.handleMenuPriceChange} required/>
                  <input type="text" placeholder="Enter Category (ex. Starter, Entree, etc)..." value={this.state.menuCategory} onChange={this.handleMenuCategoryChange} maxLength="30"required/>
                  <input type="text" placeholder="Enter Name..." value={this.state.name} onChange={this.handleMenuNameChange} maxLength="30" required/>
                  <input type="hidden" value={this.state.country}/>
                  <input type="submit" value="Add" />
              </form>
    }
  }

  handleCategoryChange(event) {
    this.setState({category: event.target.value});
  }

  handleCountryChange(event) {
    this.setState({country: event.target.value});
  }

  handleMenuCategoryChange(event) {
    this.setState({menuCategory: event.target.value});
  }

  handleMenuNameChange(event) {
    this.setState({name: event.target.value});
  }

  handleMenuPriceChange(event) {
    let tmpInput = event.target.value;
    this.setState({price: tmpInput});
  }

  onDelete(event) {
    let index = event.target.getAttribute('index');
    let tmpArray = this.state.arrayObj;
    tmpArray.splice(index, 1);
    this.setState({arrayObj: tmpArray});
  }

  trashButton(index, name) {
    return <div className="trash-button" index={index} onClick={this.props.handleDelete} name={name} ></div>;
  }

  render() {
    let title = undefined;
    let displayItems = undefined;
    let displayForm = this.getForm(this.props.children.name);

    if (this.props.children.name === 'addresses') {
      displayItems = this.state.arrayObj.map((item, index) => 
        <div className="edit-item-container" key={item.id}>
          <div><span>{item.city}, {item.country}</span></div>  
          { this.trashButton(index, this.props.children.name) }
        </div>
      )

      if (this.state.arrayObj.length > 1) {
        title = "Addresses";
      } else {
        title = "Address";
      }
    } else if (this.props.children.name === 'categories') {
      displayItems = this.state.arrayObj.map((item, index) =>
        <div className="edit-item-container" key={item.id}>
          <div><span>{item.name}</span></div>
          { this.trashButton(index, this.props.children.name) }
        </div>
      )

      if (this.state.arrayObj.length > 1) {
        title = "Categories";
      } else {
        title = "Category";
      }
    } else if (this.props.children.name === 'menus') {
      displayItems = this.state.arrayObj.map((item, index) =>
        <div className="edit-item-container" key={item.id}>
          <div><span>{item.currency.symbol} {item.price} - {item.name}</span></div>
          { this.trashButton(index, this.props.children.name) }
        </div>
      ) 

      if (this.state.arrayObj.length > 1) {
        title = "Menus";
      } else {
        title = "Menu";
      }
    };


    return (
      <div className="admin-edit">
        <div id="admin-edit-header-container">
          <div id="admin-edit-header-title"><span>Modify {title} ({this.props.children[this.props.children.name].length})</span></div>
          <div onClick={this.props.onEditClose}>Close</div>
          <div id="admin-edit-add-button">
            <div id="admin-edit-add-input">
              { displayForm }
            </div>
          </div>
        </div>
        <div id="admin-edit-body">
          {
            displayItems
          }
        </div>
      </div>
    );
  }
}

function select(state){
  return state.restaurants.restaurant;
}

export default connect(select)(Edit);

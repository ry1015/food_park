import React, { Component } from 'react';
import { connect } from "react-redux";
import { createImage, createItem, deleteItem, deleteRestaurant, updateRestaurant, updateRestaurantStatus } from '../../actions/admin/admin';
import Edit from './Edit';
import './css/admin-restaurant-info.css';

var isEqual = require('deep-equal');

// Parent: Admin.js
class RestaurantInfo extends Component {
  constructor(props) {
    super(props);
    document.body.style.overflow = "hidden";
    this.state = {
      addresses: this.props.children.addresses,
      categories: this.props.children.categories,
      menus: this.props.children.menus,
      name: this.props.children.name,
      edit: undefined,
      restaurant_image: (this.props.children.restaurant_image !== null) ? this.props.children.restaurant_image.image : undefined,
      status: this.props.children.status
    }

    this.findChange = this.findChange.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleDeleteRestaurant = this.handleDeleteRestaurant.bind(this);
    this.handleImageSubmit = this.handleImageSubmit.bind(this);
    this.handleStatusClick = this.handleStatusClick.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onChange = this.onChange.bind(this);
    this.onEdit = this.onEdit.bind(this);
    this.onEditClose = this.onEditClose.bind(this);
    this.onSave = this.onSave.bind(this);
    this.originalState = this.state;
    this.displayImage = this.displayImage.bind(this);
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (!isEqual(this.state.addresses, this.props.children.addresses) ||
        !isEqual(this.state.categories, this.props.children.categories) ||
        !isEqual(this.state.menus, this.props.children.menus) ||
        !isEqual(this.state.restaurant_image, this.props.children.restaurant_image.image) ||
        !isEqual(this.state.status, this.props.children.status)
      ){
      this.setState({
        addresses: this.props.children.addresses,
        categories: this.props.children.categories,
        menus: this.props.children.menus,
        restaurant_image: this.props.children.restaurant_image.image,
        status: this.props.children.status
      });
    };
  }

  componentWillUnmount() {
    document.body.style.overflow = null;
  }

  displayImage() {
    let emptyStyle = {
      backgroundImage: "url('/api/static/add_image.png')"
    }
    let style = {
      backgroundImage: `url(/api/static/${this.props.children.id}/${this.state.restaurant_image})`
    }

    if (this.state.restaurant_image !== undefined) {
      return <div id="admin-restaurant-info-img" style={style} >
        <input type="file" hidden onChange={this.handleImageSubmit}/>
        <div id="admin-restaurant-info-name">
          <input type="text" value={this.state.name} onChange={this.onChange} name="restaurant"/>
        </div>
      </div>
    } else {
      return <div id="admin-restaurant-info-img-empty" style={emptyStyle} >
        <input type="file" hidden onChange={this.handleImageSubmit}/>
        <div id="admin-restaurant-info-name">
          <input type="text" value={this.state.name} onChange={this.onChange} name="restaurant"/>
        </div>
      </div>
    }
  }

  findChange(event) {
    let { dispatch } = this.props;
    let payload = {}

    if (event.target[0] === undefined && !isEqual(this.originalState.name, this.state.name)) {
      payload = {
        id: this.props.children.id,
        name: this.state.name
      }
      dispatch(updateRestaurant(payload));
    }
  }

  handleDelete(event) {
    let index = event.target.getAttribute('index');
    let name = event.target.getAttribute('name');
    let payload = {
      restaurant: this.props.children.id,
      id: this.state[name][index].id
    };
    let { dispatch } = this.props;

    dispatch(deleteItem(payload, name));
  }

  handleDeleteRestaurant(event) {
    let { dispatch } = this.props;
    let id = this.props.children.id;
    dispatch(deleteRestaurant(id));
    this.props.handleClose();
  }

  handleImageSubmit(event) {
    let { dispatch } = this.props;
    let formData = new FormData();

    formData.append('image', event.target.files[0])
    formData.append('restaurant', this.props.children.id)
    dispatch(createImage(formData, this.props.children.id));
  }

  handleStatusClick(event) {
    let { dispatch } = this.props;
    let payload = {
      id: this.props.children.id,
    }
    let clickedStatus = (event.target.textContent.toLowerCase() === 'open') ? 1 : 0;

    if (this.state.status === 1 && clickedStatus === 0) {
      // dispatch close
      payload['status'] = 0;
      dispatch(updateRestaurantStatus(payload));
    } else if (this.state.status === 0 && clickedStatus === 1){
      // dispatch open
      payload['status'] = 1;
      dispatch(updateRestaurantStatus(payload));
    }
  }

  handleSubmit(event) {
    let payload = {};
    let { dispatch } = this.props;
    let name = event.target.name;

    if (name === 'addresses' && event.target[0].value !== '' && event.target[1].value !== '') {
      let city = event.target[0].value.trim();
      let country = event.target[1].value.trim();
      payload = {
        'city': city,
        'country': country,
        'restaurant': this.props.children.id
      };
      dispatch(createItem(payload, name));

    } else if (name === 'categories' && event.target[0].value !== '') {
      payload = {
        'name': event.target[0].value.trim(),
        'restaurant': this.props.children.id
      };
      dispatch(createItem(payload, name));

    } else if (name === 'menus' && event.target[0].value !== '' && event.target[1].value !== '' && event.target[2].value !== '') {
      payload = {
        'category': event.target[1].value.trim(),
        'country': event.target[3].value.trim(),
        'name': event.target[2].value.trim(),
        'price': event.target[0].value,
        'restaurant': this.props.children.id
      }
      dispatch(createItem(payload, name));
    }

    event.preventDefault();
  }

  onChange(event) {
    if (event.target.name === 'restaurant') {
      this.setState({name: event.target.value});
    }
  }

  onEdit(event) {
    this.setState({edit: event.target.getAttribute('name')})
  }

  onEditClose() {
    this.setState({edit: undefined});
  }

  onSave(event) {
    if (!isEqual(this.originalState, this.state)) {
      this.findChange(event);
      this.props.onSave(this.state);
      this.props.handleClose();
    } else {
      this.props.handleClose();
    }
  }

  render() {
    let { menus } = this.state;
    let uniqueMenu = [...new Set(menus.map(menu => ({'id': menu.id, 'name': menu.name, 'currency': menu.currency, 'price': menu.price})))];
    let editObj = {};
    let getImage = this.displayImage();

    if (this.state.edit !== undefined) {
      editObj[this.state.edit] = this.state[this.state.edit];
      editObj['name'] = this.state.edit;
    }
    
    return (
      <div className="admin-restaurant-container">
        {
          this.state.edit !== undefined && this.state.edit &&
          <Edit onEditClose={this.onEditClose} handleSubmit={this.handleSubmit} handleDelete={this.handleDelete}>{editObj}</Edit>
        }
        <div id="admin-restaurant-info">
          {/* Header */}
          <div id="admin-restaurant-info-header">
            <div id="admin-restaurant-info-header-title">
              <div><span>Edit Restaurant</span></div>
              <div id="admin-restaurant-info-header-title-trash-button" onClick={this.handleDeleteRestaurant}></div>
            </div>
            <div id="admin-restaurant-info-header-buttons">
              <div id="admin-restaurant-info-header-save-button" onClick={this.onSave}></div>
              <div id="admin-restaurant-info-header-close-button" onClick={this.props.handleClose}></div>
            </div>
          </div>

          {/* Body */}
          <div className="admin-restaurant-info-body">
            <label>
              {getImage}
            </label>

            {/* Status*/}
            <div>
              <div id="admin-retaurant-info-status">
                <label>Status</label>
                {
                  this.state.status === 1 ? (<div><span onClick={this.handleStatusClick}><b>Open</b></span> <span onClick={this.handleStatusClick}>Closed</span></div>) : (<div><span onClick={this.handleStatusClick}>Open</span> <span onClick={this.handleStatusClick}><b>Closed</b></span></div>)
                }
              </div>
            </div>
            {/* Address */}
            <div>
              <div id="admin-restaurant-info-address-label">
                <label>Addresses</label>
                <div id="admin-restaurant-info-address-edit" onClick={this.onEdit} name="addresses"></div>
              </div>
              <div>
              {
                this.state.addresses.map((address, index) =>
                  <div className="admin-restaurant-info-address" key={address.id}><span>{address.city}, {address.country}</span></div>
                )
              }
              </div>
            </div>

            {/* Category */}
            <div>
              <div id="admin-restaurant-info-categories-label">
                <label>Categories</label>
                <div id="admin-restaurant-info-category-edit" onClick={this.onEdit} name="categories"></div>
              </div>
              {
                this.state.categories.map((category, index) =>
                  <div className="admin-restaurant-info-category" key={category.id}><span>{category.name}</span></div>
                )
              }
            </div>

            {/* Menu */}
            <div>
              <div id="admin-restaurant-info-menu-label">
                <label>Menu</label>
                <div id="admin-restaurant-info-menu-edit" onClick={this.onEdit} name="menus"></div>
              </div>
              <div id="admin-restaurant-info-menu-list">
                {
                  uniqueMenu.map((item, index) =>
                    <div className="admin-restaurant-info-menu" key={item.id}><span>{item.currency.symbol} </span><span>{item.price}</span><br/><span>{item.name}</span></div>
                  )
                }
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function select(state){
  return state.restaurants;
}

export default connect(select)(RestaurantInfo);

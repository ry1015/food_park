import React, { Component } from 'react';
import { connect } from "react-redux";
// import logo from './logo.svg';
// import './App.css';
import AdminHeader from './AdminHeader';
import AdminBody from './AdminBody';
import AdminLogin from './AdminLogin';
import RestaurantInfo from './RestaurantInfo';
import './css/admin.css';
import { closeRestaurantInfo, fetchRestaurants, fetchUser, verifyToken } from '../../actions/admin/admin';

class Admin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      creds: {
        username: 'admin', 
        password: 'Spring20!6'
      },
      restaurant: this.props.restaurant
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.onSave = this.onSave.bind(this);
    this.closeRestaurantInfo = this.closeRestaurantInfo.bind(this);
  }

  componentDidMount() {
    let { tokenVerified } = this.props.login;
    if (!tokenVerified) {
      let { dispatch } = this.props;
      dispatch(verifyToken());
    }
  }

  closeRestaurantInfo() {
    let { dispatch } = this.props;
    dispatch(closeRestaurantInfo());
  }

  handleSubmit(creds) {
    let { dispatch } = this.props;
    dispatch(fetchUser(this.state.creds));
  }

  onSave(payload) {
    let { dispatch } = this.props;
    dispatch(fetchRestaurants());
  }

  render() {
    let { isLoggedIn } = this.props.login;
    let { restaurant } = this.props.restaurant;

    return (
      <div className="admin">
        <AdminHeader />
        {
          isLoggedIn ? (
            <AdminBody />
          ) : (
            <AdminLogin handleSubmit={this.handleSubmit}/>
          )
        }
        {
          restaurant !== undefined &&
          <RestaurantInfo handleClose={this.closeRestaurantInfo} onSave={this.onSave}>{restaurant}</RestaurantInfo>
        }
      </div>
    );
  }
}

function select(state){
  return {login: state.login, restaurant: state.restaurants};
}

export default connect(select)(Admin);


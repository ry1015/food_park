import React, { Component } from 'react';
import { withRouter } from 'react-router-dom'
import InfiniteScroll from 'react-infinite-scroller';
import axios from 'axios';
import { connect } from "react-redux";
import AdminFilter from './AdminFilter';
import Restaurant from './Restaurant';
import { fetchRestaurant, fetchRestaurants } from '../../actions/admin/admin';
import './css/admin-body.css';
import { getCookie } from '../../actions/admin/admin';

const api = {
    baseUrl: 'http://127.0.0.1:8000',
    config: {
      headers: {
        'X-CSRFToken': getCookie('csrftoken'),
        'Access-Control-Allow-Origin': '*',
        'Authorization': "Bearer " + localStorage.getItem("ut")
      }
    }
};

const isEqual = require('deep-equal');

class AdminBody extends Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 0,
      next: null,
      previous: null,
      results: [],
      userInput: "",
      restaurant: undefined,
      hasMoreItems: true
    };

    this.handleRestaurantClick = this.handleRestaurantClick.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
    this.loadItems = this.loadItems.bind(this);
    this.userInput = "";
  }

  componentDidMount() {
    let { dispatch } = this.props;
    dispatch(fetchRestaurants());
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (!isEqual(this.props.restaurants.results, this.state.results)) {
      var newState = this.props.restaurants;
      if (newState.next) {
        newState["hasMoreItems"] = true;
      }
      this.setState(newState);
    }

    if (this.props.restaurant !== this.state.restaurant) {
      this.setState({restaurant: this.props.restaurant});
    }
  }

  handleSearch(input) {
    this.setState({userInput: input.target.value});
  }

  handleRestaurantClick(id) {
    let { dispatch } = this.props;
    dispatch(fetchRestaurant(id));
    console.log(this.props.history);
  }

  loadItems(page) {
    var url = this.state.next;

    axios.get(
      url,
      api.config
    )
    .then(response => {
      var newResults = this.state.results;
      response.data.results.map((rest) => 
        newResults.push(rest)
      );

      if (response.data.next) {
        var newState = this.props.restaurants;
        newState["count"] = response.data.count;
        newState["next"] = response.data.next;
        newState["previous"] = response.data.previous;
        newState["results"] = newResults;
        this.setState(newState);
      } else {
        this.setState({hasMoreItems: false});
      }
    })
    .catch(error => {
      if (error.response !== undefined) {
        window.location.reload();
        console.log(error.response);
      }
      throw(error);
    });
  }

  render() {
    let displayRestaurants = undefined;
    if (this.state.userInput !== "") {
      let tmpRestaurants = [];
      for (var rest in this.state.results) {
        if (this.state.results[rest]["name"].toLowerCase().includes(this.state.userInput)) {
          tmpRestaurants.push(this.state.results[rest]);
        }
      }
      displayRestaurants = tmpRestaurants.map((rest, index) => 
        <Restaurant key={rest.id} rid={rest.id} handleRestaurantClick={this.handleRestaurantClick}>{rest}</Restaurant>
      );
    } else {
      displayRestaurants = this.state.results.map((rest, index) => 
        <Restaurant key={rest.id} rid={rest.id} handleRestaurantClick={this.handleRestaurantClick}>{rest}</Restaurant>
      );
    }

    return (
      <InfiniteScroll 
        pageStart={0}
        loadMore={this.loadItems}
        hasMore={this.state.hasMoreItems}
        className="admin-body">

        <AdminFilter handleSearch={this.handleSearch}/>
        {displayRestaurants}

      </InfiniteScroll>
    );
  }
}

function select(state){
  return state.restaurants;
}

export default withRouter(connect(select)(AdminBody));

import React, { Component } from 'react';
// import logo from './logo.svg';
// import './App.css';
import './css/header.css';

class Header extends Component {
  render() {
    return (
      <div className="header">
        <div id="header-wrapper">
          <div id="title">Food Park</div>
          <div id="order-button">Cart</div>
        </div>
      </div>
    );
  }
}

export default Header;

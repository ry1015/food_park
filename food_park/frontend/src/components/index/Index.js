import React, { Component } from 'react';
// import logo from './logo.svg';
// import './App.css';
import Header from './Header';
import Body from './Body';
import './css/index.css';

class Index extends Component {
  render() {
    return (
      <div className="index">
        <Header />
        <Body />
      </div>
    );
  }
}

export default Index;

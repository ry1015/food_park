import React, { Component } from 'react';
// import logo from './logo.svg';
// import './App.css';
import Restaurants from './Restaurants';
import './css/body.css';

class Body extends Component {
  render() {
    return (
      <div className="body">
        <div id="body-title">
          Available Food Trucks
        </div>
        <Restaurants />
      </div>
    );
  }
}

export default Body;

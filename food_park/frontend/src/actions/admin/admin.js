import axios from 'axios';
import { CLOSE_RESTAURANT_INFO, FETCH_RESTAURANT_INFO, FETCH_RESTAURANTS, LOGIN_SUCCESS, TOKEN_VERIFIED } from '../index'

export const getCookie = (name) => {
  let cookieValue = null;
  if(document.cookie && document.cookie !== '') {
    let cookies = document.cookie.split(';');
    for(let i = 0; i < cookies.length; i++) {
      let cookie = cookies[i].trim();
      if(cookie.substring(0, name.length + 1) === (name + '=')) {
        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
        break;
      }
    }
  }
  return cookieValue;
}

const apiUrl = 'http://127.0.0.1:8000';
let token = localStorage.getItem("ut");
const config = {
  headers: {
    'Access-Control-Allow-Origin': '*',
    'Authorization': `Bearer ${token}`,
    'Content-Type': 'application/json',
    'X-CSRFToken': getCookie('csrftoken')
  }
};

export function closeRestaurantInfo() {
  return {
    type: CLOSE_RESTAURANT_INFO,
    payload: {}
  }
}

export function fetchRestaurantSuccess(payload) {
  return {
    type: FETCH_RESTAURANTS,
    payload: payload
  }
}

export function fetchRestaurantInfoSuccess(payload) {
  return {
    type: FETCH_RESTAURANT_INFO,
    payload: payload
  }
}

export function loginSuccess(status) {
  return {
    type: LOGIN_SUCCESS,
    payload: {
      status
    }
  }
}

export function tokenVerified() {
  return {
    type: TOKEN_VERIFIED,
    payload: {}
  }
}

export const createImage = (payload, id) => {
  let newConfig = config;
  newConfig['headers']['Content-Type'] = 'multipart/form-data';

  return(dispatch) => {
    return axios({
      'method': 'POST',
      'url': `${apiUrl}/api/v1/restaurant_images`,
      'headers': newConfig.headers,
      'data': payload
    }
    )
    .then(response => {
      // dispatch(updateAll(response.data.restaurant));
      console.log(response.data);
      // console.log(payload);
      dispatch(updateAll(id));
    })
    .catch(error => {
      console.log(error.response);
      throw(error);
    });
  };
};

export const createItem = (payload, name) => {
  return(dispatch) => {
    return axios({
      'method': 'POST',
      'url': `${apiUrl}/api/v1/${name}`,
      'headers': config.headers,
      'data': payload
    })
    .then(response => {
      dispatch(updateAll(response.data.restaurant));
    })
    .catch(error => {
      console.log(error.response);
      throw(error);
    });
  };
};

export const deleteItem = (obj, name) => {
  return (dispatch) => {
    return axios({
      'method': 'DELETE',
      'url': `${apiUrl}/api/v1/${name}/${obj.id}`,
      'headers': config.headers
    })
    .then(response => {
      dispatch(updateAll(obj.restaurant));
    })
    .catch(error => {
      console.log(error.response);
      throw(error);
    });
  }
}

export const deleteRestaurant = (id) => {
  return (dispatch) => {
    return axios({
      'method': 'DELETE',
      'url': `${apiUrl}/api/v1/restaurants/${id}`,
      'headers': config.headers
    })
    .then(response => {
      dispatch(fetchRestaurants());
    })
    .catch(error => {
      console.log(error.response);
      throw(error);
    });
  }
}

export const fetchRestaurant = (id) => {
  let token = localStorage.getItem("ut");
  config["headers"]["Authorization"] = `Bearer ${token}`;
  return(dispatch) => {
    return axios.get(
      `${apiUrl}/api/v1/restaurants/${id}`,
      config
    )
    .then(response => {
      dispatch(fetchRestaurantInfoSuccess(response.data));
    })
    .catch(error => {
      console.log(error.response.request.response);
      throw(error);
    });
  };
};

export const fetchRestaurants = () => {
  let token = localStorage.getItem("ut");
  config["headers"]["Authorization"] = `Bearer ${token}`;
  return (dispatch) => {
    return axios.get(
      `${apiUrl}/api/v1/restaurants`,
      config
    )
    .then(response => {
      dispatch(fetchRestaurantSuccess(response.data));
    })
    .catch(error => {
      console.log(error.response);
      throw(error);
    });
  };
};

export const fetchUser = (creds) => {
  return (dispatch) => {
    return axios.post(
      `${apiUrl}/api/v1/token/obtain`,
      creds,
      config
    )
    .then(response => {
      localStorage.setItem('ut', response.data.access);
      localStorage.setItem('utr', response.data.refresh);
      dispatch(loginSuccess(response.status));
    })
    .catch(error => {
      console.log(error.response.request.response);
      throw(error);
    });
  };
};

export const updateAll = (id) => {
  return (dispatch) => {
    dispatch(fetchRestaurant(id));
    dispatch(fetchRestaurants());
  };
};

export const updateRestaurant = (payload) => {
  return (dispatch) => {
    return axios({
      'method': 'PATCH',
      'url':  `${apiUrl}/api/v1/restaurants/${payload.id}`,
      'headers': config.headers,
      'data': payload
    })
    .then(response => {
      dispatch(fetchRestaurants());
    })
    .catch(error => {
      console.log(error.response.request.response);
      throw(error);
    });
  };
};

export const updateRestaurantStatus = (payload) => {
  return (dispatch) => {
    return axios({
      'method': 'PATCH',
      'url':  `${apiUrl}/api/v1/restaurants/${payload.id}`,
      'headers': config.headers,
      'data': payload
    })
    .then(response => {
      dispatch(updateAll(response.data.id));
    })
    .catch(error => {
      console.log(error.response.request.response);
      throw(error);
    });
  };
};

export const verifyToken = () => {
  let payload = {
    token: localStorage.getItem("ut")
  };
  return (dispatch) => {
    return axios.post(
      `${apiUrl}/api/v1/token/verify`,
      payload,
      config
    )
    .then(response => {
      dispatch(tokenVerified());
    })
    .catch(error => {
      // console.log(error.response);
      // throw(error);
    });
  };
};
